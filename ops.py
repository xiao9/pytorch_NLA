import torch
import torch.nn as nn
import numpy as np
import scipy.sparse as sp


def normalize_adj(adj):
    """Symmetrically normalize adjacency matrix."""
    adj = adj.to_dense().cpu().numpy()
    adj = adj + 2 * sp.eye(adj.shape[0])
    un_normed_adj = adj
    adj = sp.coo_matrix(adj)
    rowsum = np.array(adj.sum(1))
    d_inv_sqrt = np.power(rowsum, -0.5).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.diags(d_inv_sqrt)
    adj = adj.dot(d_mat_inv_sqrt).transpose().dot(d_mat_inv_sqrt).tocoo()
    un_A = torch.FloatTensor(un_normed_adj).cuda()
    A = torch.FloatTensor(adj.todense()).cuda()
    return un_A, A
    # indices = torch.LongTensor(np.vstack((adj.row, adj.col)))
    # values = torch.FloatTensor(adj.data)
    # shape = torch.Size(adj.shape)
    # return torch.sparse.FloatTensor(indices, values, shape).cuda()


def normalize_adj_torch(mx):
    mx = mx.to_dense()
    rowsum = mx.sum(1)
    r_inv_sqrt = torch.pow(rowsum, -0.5).flatten()
    r_inv_sqrt[torch.isinf(r_inv_sqrt)] = 0.
    r_mat_inv_sqrt = torch.diag(r_inv_sqrt)
    mx = torch.matmul(mx, r_mat_inv_sqrt)
    mx = torch.transpose(mx, 0, 1)
    mx = torch.matmul(mx, r_mat_inv_sqrt)
    return mx


class GraphUnet(nn.Module):

    def __init__(self, ks, in_dim, out_dim, dim=48):
        super(GraphUnet, self).__init__()
        self.ks = ks
        self.start_gcn = GCN(in_dim, dim)
        self.bottom_gcn = GCN(dim, dim)
        self.end_gcn = GCN(2*dim, out_dim)
        self.down_gcns = []
        self.up_gcns = []
        self.pools = []
        self.unpools = []
        self.l_n = len(ks)
        for i in range(self.l_n):
            self.down_gcns.append(GCN(dim, dim))
            self.up_gcns.append(GCN(dim, dim))
            self.pools.append(GraphPool(ks[i], dim))
            self.unpools.append(GraphUnpool())

    def forward(self, A, X):
        adj_ms = []
        indices_list = []
        down_outs = []
        X = self.start_gcn(A, X)
        org_X = X
        for i in range(self.l_n):
            X = self.down_gcns[i](A, X)
            adj_ms.append(A)
            down_outs.append(X)
            A, X, idx = self.pools[i](A, X)
            indices_list.append(idx)
        X = self.bottom_gcn(A, X)
        for i in range(self.l_n):
            up_idx = self.l_n - i - 1
            A, idx = adj_ms[up_idx], indices_list[up_idx]
            A, X = self.unpools[i](A, X, idx)
            X = self.up_gcns[i](A, X)
            X = X.add(down_outs[up_idx])
        X = torch.cat([X, org_X], 1)
        X = self.end_gcn(A, X)
        return X


class GraphUnpool(nn.Module):

    def __init__(self):
        super(GraphUnpool, self).__init__()

    def forward(self, A, X, idx):
        new_X = torch.zeros([A.shape[0], X.shape[1]]).cuda()
        new_X[idx] = X
        return A, new_X


class GraphPool(nn.Module):

    def __init__(self, k, in_dim):
        super(GraphPool, self).__init__()
        self.k = k
        self.proj = nn.Linear(in_dim, 1).cuda()
        self.tanh = nn.Tanh()

    def forward(self, A, X):
        scores = self.proj(X)
        scores = torch.abs(scores)
        scores = torch.squeeze(scores)
        scores = self.tanh(scores/100)
        num_nodes = A.shape[0]
        values, idx = torch.topk(scores, int(self.k*num_nodes))
        new_X = X[idx, :]
        A = A[idx, :]
        A = A[:, idx]
        return A, new_X, idx


class GCN(nn.Module):

    def __init__(self, in_dim, out_dim, p):
        super(GCN, self).__init__()
        self.proj = nn.Linear(in_dim, out_dim).cuda()
        self.drop = nn.Dropout(p=p)

    def forward(self, A, X):
        X = self.drop(X)
        X = torch.matmul(A, X)
        X = self.proj(X)
        return X


class GAT(nn.Module):

    def __init__(self, in_dim, out_dim):
        super(GAT, self).__init__()
        self.in_dim, self.out_dim = in_dim, out_dim
        self.proj = nn.Linear(in_dim, in_dim+out_dim).cuda()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, A, X, k, v=1):
        qs = X
        proj_X = self.proj(X)
        ks, vs = torch.split(proj_X, [self.in_dim, self.out_dim], dim=1)
        if v == 0:
            X = self.dot_prod(A, qs, ks, vs)
        else:
            X = self.bi_dot_prod(A, qs, ks, vs)
        return X

    def dot_prod(self, A, qs, ks, vs):
        ks = torch.transpose(ks, 0, 1)
        weights = torch.matmul(qs, ks)
        weights = weights / np.sqrt(qs.shape[0])
        weights = torch.mul(weights, A)
        outs = torch.matmul(weights, vs)
        return outs

    def bi_dot_prod(self, A, qs, ks, vs):
        # NxN, Nxd1, Nxd1, Nxd2
        ks = torch.transpose(ks, 0, 1)
        weights = torch.matmul(ks, A)
        weights = torch.matmul(weights, vs)
        weights = weights / np.sqrt(ks.shape[0])
        weights = self.softmax(weights)
        outs = torch.matmul(qs, weights)
        return outs


class GHAT(nn.Module):

    def __init__(self, in_dim, out_num):
        super(GHAT, self).__init__()
        self.drop = nn.Dropout(p=0.3)
        self.in_dim, self.out_num = in_dim, out_num
        self.proj = nn.Linear(in_dim, 1+in_dim+out_num).cuda()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, A, X, k, v=0):
        qs = X.unsqueeze(1)
        proj_X = self.proj(X)
        scores, ks, vs = torch.split(
            proj_X, [1, self.in_dim, self.out_num], dim=1)
        scores = torch.abs(scores)
        scores = self.softmax(scores/100)
        scores = torch.transpose(scores, 0, 1)
        scores = torch.mul(A, scores)
        values, idx = torch.topk(scores, k)
        ks_1d = ks[idx.view(-1)]
        vs_1d = vs[idx.view(-1)]
        ks = ks_1d.view(-1, k, self.in_dim)
        vs = vs_1d.view(-1, k, self.out_num)
        values = torch.unsqueeze(values, -1)
        vs = torch.mul(vs, values)
        if v == 0:
            X = self.dot_prod(qs, ks, vs)
        else:
            X = self.bi_dot_prod(qs, ks, vs)
        X.squeeze_(1)
        return X

    def dot_prod(self, qs, ks, vs):
        # Nx1xd, Nxkxd, Nxkxd
        ks = torch.transpose(ks, 1, 2)
        weights = torch.matmul(qs, ks)
        weights = weights / np.sqrt(qs.shape[0])
        weights = self.softmax(weights)
        outs = torch.matmul(weights, vs)
        return outs

    def bi_dot_prod(self, qs, ks, vs):
        ks = torch.transpose(ks, 1, 2)
        weights = torch.matmul(ks, vs)
        weights = weights / np.sqrt(ks.shape[0])
        weights = self.softmax(weights)
        outs = torch.matmul(qs, weights)
        return outs


class GraphAttNet(nn.Module):
    def __init__(self, in_dim, out_dim, dim=48, att_dim=12):
        super(GraphAttNet, self).__init__()
        self.gats = []
        self.cgats = []
        self.gcns = []
        self.l_n = 4
        self.k = 8
        p = 0.5
        self.start_gcn = GCN(in_dim, dim, p)
        self.end_gcn = GCN(dim+att_dim*self.l_n, out_dim, p)
        for i in range(self.l_n):
            self.gats.append(GHAT(dim+i*att_dim, att_dim))
            self.gcns.append(GCN(att_dim, att_dim, p))

    def forward(self, un_A, A, X):
        X = self.start_gcn(A, X)
        for i in range(self.l_n):
            cur_X = self.gats[i](un_A, X, self.k)
            cur_X = self.gcns[i](A, cur_X)
            X = torch.cat([X, cur_X], 1)
        X = self.end_gcn(A, X)
        return X
